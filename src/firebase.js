/* eslint-disable */

import firebase from 'firebase/app';
import 'firebase/auth';
import "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: 'AIzaSyAjRWhVuHYcX-boB9azIik7o5deeRMGwpI',
	authDomain: 'reddit-clone-3a829.firebaseapp.com',
	databaseURL: 'https://reddit-clone-3a829.firebaseio.com',
	projectId: 'reddit-clone-3a829',
	storageBucket: 'reddit-clone-3a829.appspot.com',
	messagingSenderId: '8153665	8061',
	appId: '1:81536658061:web:07e931da145a79931550c4',
	measurementId: 'G-XLD4VJSZJK'
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;
