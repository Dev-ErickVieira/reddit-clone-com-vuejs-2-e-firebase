/* eslint-disable */
import firebase from '@/firebase';

const state = {
	user: {},
	isLoggedIn: false,
};	

const mutations = {
	setUser(state, user) {
		state.user = user != null ? user : {};
		state.isLoggedIn = user != null ? true : false;
	},
};

const actions = {
	async	login() {
		const provider = new firebase.auth.GoogleAuthProvider();
		await firebase.auth().signInWithPopup(provider);
	},
	async logOut({ commit }) {
		commit('loading/setLoader', true);
		await firebase.auth().signOut();
		commit('loading/setLoader', false);
	}
};

const getters = {
	logged(state) {
		return state.isLoggedIn;
	},
	userId(state) {
		return state.user.id
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
};

